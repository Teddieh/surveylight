/*
 * @package    mod_surveylight
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * @module mod_surveylight/surveysubmit
 */

define(['jquery', 'core/ajax'],
    function($, ajax) {

        return {
            init: function(cmid) {


                $('button.survey-submit[name="submit_survey"][cmid="' + cmid + '"]').click(function(){
                    var answer = "";
                    var i = 0;

                    var qid = $(this).attr('qid');

                    $('input.survey-question[cmid="' + cmid + '"][qid="' + qid + '"]').each(function(){
                        if($(this).prop('checked')){
                            if(i > 0){
                                answer += ";";
                            }
                            answer += $(this).attr('value');
                            i++;
                        }
                    });
                    var promises = ajax.call([
                        { methodname: 'mod_surveylight_survey', args: {answer: answer,
                                cmid: $(this).attr('cmid'), qid: $(this).attr('qid')} }
                    ]);

                    promises[0].done(function(response) {
                        //remove the question-thin
                        var qblock = $('div.question[qid="' + qid + '"]');
                        qblock.slideUp('fast');

                        var ablock = $('div.answers[qid="' + qid + '"]');
                        ablock.slideDown('fast');

                        var answers = response['answers'].split(';');
                        var answers1 = response['answers1'].split(";");
                        var sumanswer = response['sumanswers'];

                        for(var i = 0; i < answers.length; i++){
                            var answer = $('label.answer[qid="' + qid + '"][ansindex="' + i + '"]');

                            var text = '<div class="statbox"><div class="statbox" style="position: absolute; margin-left: 36.09%">' + answers[i]+ '%</div>' +
                                '<div class="surv-meter"><div class="surv-meter-bar surv-meter-filled" style="width:'
                                + answers[i] + '%" aria-label="' + answers[i] + '%"> '+'</div></div>'
                                + '</div>';
                            if((answers.length-2) == i){
                                total_answer = '<div class="total_ans" style="font-weight: bolder; float:right; margin-top: 20px;" >Antworten gesamt: ' +  sumanswer + '</div>';
                                text += total_answer;
                            }
                            answer.html(text);
                        }
                    }).fail(function() {

                    });
                });

                // activates submit button with at least one selected answer
                // deactivates submut button with no selected answer
                $('input.survey-question[cmid="' + cmid + '"]').change(function(){

                    var checked = false;
                    $('input.survey-question[cmid="' + cmid + '"]').each(function(){
                        if($(this).prop('checked')){
                            checked = true;
                        }
                    });
                    if(!checked){
                        $('button.survey-submit[name="submit_survey"][cmid="' + cmid + '"][qid="'
                            + $(this).attr('qid') + '"]').attr('disabled', 'disabled');
                    }else{
                        $('button.survey-submit[name="submit_survey"][cmid="' + cmid + '"][qid="'
                            + $(this).attr('qid') + '"]').removeAttr('disabled');
                    }
                });
            }
        };
    });