<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_surveylight
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/locallib.php');

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function surveylight_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the mod_surveylight into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance An object from the form.
 * @param mod_surveylight_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function surveylight_add_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timecreated = time();

    $id = $DB->insert_record('surveylight', $moduleinstance);

    /*

    $update = new stdClass();
    $update->id = $id;
    $update->coursemodule = $moduleinstance->coursemodule;

    $cmid = $DB->update_record('surveylight', $update);

    */

    return $id;
}

/**
 * Updates an instance of the mod_surveylight in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_surveylight_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function surveylight_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('surveylight', $moduleinstance);
}

/**
 * Removes an instance of the mod_surveylight from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function surveylight_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('surveylight', array('id' => $id));

    if (!$exists) {
        return false;
    }

    $DB->delete_records('surveylight', array('id' => $id));

    return true;
}


/**
 * Embed surveylight-content - oncampus mod
 *
 * @param cm_info $cm
 */


function surveylight_cm_info_view(cm_info $cm) {

    global $USER, $CFG, $DB, $PAGE;
    global $OCSURVLOADED;

    // Q&D - Workaround for oncampus.de (double call)
    if(!isset($OCSURVLOADED)){
        $OCSURVLOADED = array($cm->id => true);
    }else {
        if($OCSURVLOADED[$cm->id]){
            return;
        } else {
            $OCSURVLOADED = array($cm->id => true);
        }
    }
    
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('surveylight', array('id' => $cm->instance), '*', MUST_EXIST);

    $modulecontext = context_module::instance($cm->id);

    $questions = $DB->get_records('surveylight_questions', ['survey' => $moduleinstance->coursemodule]);

    if($DB->record_exists('surveylight_answers', array('userid' => $USER->id, 'survey' => $cm->id))){
        $answered = true;
    }

    echo new_create_html($questions, $cm, $answered);
    $test = $_POST('');

    $PAGE->requires->js_call_amd('mod_surveylight/surveysubmit', 'init', array($cm->id));
}

/**
 * @param $course
 * @param $cm
 * @param $userid
 * @param $type
 * @return bool
 * @throws Exception
 */
function surveylight_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get survey details
    if (!($survey = $DB->get_record('surveylight', array('id' => $cm->instance)))) {
        throw new Exception("Can't find purchase {$cm->instance}");
    }
    return $DB->record_exists('surveylight_answers', array('userid' => $userid, 'survey' => $survey->coursemodule));
}

/**
 * Remove activity link - oncampus mod
 *
 * @param cm_info $cm
 */
function surveylight_cm_info_dynamic(cm_info $cm) {
    global $USER;

    if (isset($USER->editing) && $USER->editing != 1) {
        $cm->set_no_view_link();
    }
}

