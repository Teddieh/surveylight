<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_surveylight
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(__DIR__ . '/checkform.php');
require_once(__DIR__ . '/radioform.php');


function new_create_html($questions, $cm, $answered = false){
    global $DB;

    $html = "<li class='activity survey modtype_survey'>";

    foreach ($questions as $que) {
        $temp = "";
        if($que->ofbtn == 1){
            $html .= "<scrip> 
                  
                              
                       </scrip>";
            $html .= "<scrip> $(document)</scrip>";

            $html .= "<div class='backgroundbox' style='background-color: $que->colorcode; color: #fff;  padding-right: 5%;'>";
            $html .=    "<div class='firstbox'>";
            $html .=        "<img src='/mod/surveylight/pix/vote.png'  class='ig' >";
            $html .=        "<div class='container_intro'>";
            $html .=            "<div class='title_box' style='padding-left:30px;font-weight: 300; font-size: 25px;white-space: pre-wrap;'>$que->title </h1> </div>";
            $html .=            "<div class='intro_box' style='padding-left:30px;font-weight: 300; font-size: 16px;'>$que->intro </h3></div>";
            $html .=        "</div>";
            $html .=    "</div>";
            $html .= '</div>';
            $html .= "<form onsubmit=''>";
            $html .= "<textarea id='test' cols='50' rows='10'>  </textarea>";
            $html .= "<button class='btn btn-primary survey-submit' id='opbtn' name='submit' onclick='' style='margin-left: 50px; margin-bottom: 20px;' disabled>".get_string('submit')."</button>";
            $hmtl .= "</form>";
        } else {

        if($que->headerbtn != 1){
            $html .= "<div class='backgroundbox' style='background-color: $que->colorcode; color: #fff;  padding-right: 5%;'>";
            $html .=    "<div class='firstbox'>";
            $html .=        "<img src='/mod/surveylight/pix/vote.png'  class='ig' >";
            $html .=        "<div class='container_intro'>";
            $html .=            "<div class='title_box' style='padding-left:30px;font-weight: 300; font-size: 25px;white-space: pre-wrap;'>$que->title </h1> </div>";
            $html .=            "<div class='intro_box' style='padding-left:30px;font-weight: 300; font-size: 16px;'>$que->intro </h3></div>";
            $html .=        "</div>";
            $html .=    "</div>";
            $html .= '</div>';
        } else if ($que->headerbtn == 1 and $que->scalabtn != 1){
            $html .= $que->intro;
        }

        if($que->scalabtn == 1){
            $html .= '<div class="scalatest" style="padding-left: 20px">'. $que->scalatitle .'</div>';
            $html .= '<hr style="width: 80%; float:left;">';
            $temp = $que->survey;
        }
        $options = explode(';', $que->options);
        if(!$answered){
            $html .= "<div class='question' qid='$que->id'>";

            $formurl = new moodle_url('/mod/surveylight/view.php?id=' . $cm->id);

            $customdata = array('cmid' => $cm->id);
            $customdata['answers'] = $options;

            $i = 0;
            foreach($options as $option){
                if(!empty($option)) {
                    $html .= "<label class='radio-inline answer-option' style='padding-left: 15px;' ><input type='$que->multi' class='survey-question' style='margin-right: 15px;background-color: #000000;' name='answer_$que->id' cmid='$cm->id' qid='$que->id' value='$i'/>$option</label>";
                    $i++;
                }
            }
            $html .= "<div class='buttonbox'>";
            $html .= "<button class='btn btn-primary survey-submit' name='submit_survey' style='margin-left: 50px; margin-bottom: 20px' cmid='$cm->id' qid='$que->id' disabled>".get_string('submit')."</button>";
            $html .= "</div>";
            $html .= "</div>";
        }


        $answers = $DB->get_records('surveylight_answers', array('survey' => $cm->id, 'question' => $que->id));
        $answercount = array();

        foreach($options as $id => $option){
            $answercount[$id] = 0;
            $answercount1[$id] = 0;
        }

        $count = 0;

        foreach($answers as $answer){
            $answeroptions = explode(';', $answer->answer1);
            foreach($answeroptions as $option){
                $answercount[$option]++;
                $answercount1[$option]++;
                $count++;
            }
        }

        foreach($answercount as $id => $ac){
            $answercount[$id] = round(($ac / $count) * 100, 2);
            $answercount1[$id] = $ac;
        }



        if($answered){
            $style = "style='display:block;'";
        } else {
            $style = "style='display:none;'";
        }

        $html .= "<div class='answers' qid='$que->id' $style>";

        $i = 0;
        $totalcount = 0;

        foreach($options as $id => $option){
            if(!empty($option)) {
                $html .= "<div class='answer-div' style='padding-left: 12px;' qid='$que->id' ansindex='$i'>";
                #$html .= "<span><label class='radio-inline answer-option'>$option : <label class='qid'> $testarray[$i]</div></label><label class='answer' ansindex='$i' qid='$que->id'>";
                $html .= "<span>
                            <label class='radio-inline answer-option'>$option :</label>
                           <label class='answer' ansindex='$i' qid='$que->id'>";
                $html .= "<div class='statbox'>";
                $html .= "<div class='statbox' style='position: absolute; margin-left: 36.09%'> $answercount[$id]%</div>";
                $html .=    '<div class="surv-meter">
                                <div class="surv-meter-bar surv-meter-filled" style="width:' . $answercount[$id] . '%" aria-label="' . $answercount[$id] . '%"> ' .   "</div>";
                $html .=    "</div>";
                $html .= "</div>";

                $totalcount = $totalcount + 1;

                if(sizeof($options)-1 == $totalcount){
                    $sumanswers = 0;

                    for($indexoftotal = 0; $indexoftotal<sizeof($answercount1); $indexoftotal++){
                        $sumanswers = $sumanswers + $answercount1[$indexoftotal];
                    }
                    $html .= "<div class='ppl_ans'>";
                    $html .= "<div class='total_ans' style='font-weight: bolder;'>".get_string('total_answer', 'mod_surveylight'). $sumanswers."</div>";
                    $html .= "</div>";
                    $html .= "</div>";
                }
                $html .= "</label></span>";
                $html .= "</div>";
                $i++;
            }
        }
    }
    }

    $html .= "</li>";
    return $html;
}

