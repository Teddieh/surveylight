<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add question to survey
 *
 * @package     mod_surveylight
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/lib.php');
require_once('addquestion_form.php');

// Course_module ID
$id = optional_param('id', 0, PARAM_INT);
$coursemodule = optional_param('cm', 0, PARAM_INT);
$action = optional_param('action', 'create', PARAM_RAW);

$formurl = new moodle_url('/mod/surveylight/addquestion.php?id=' . $id . '&coursemodule=' . $coursemodule);
$customdata = array('id' => $id, 'coursemodule' => $coursemodule);
$globalquestion = $DB->get_record('surveylight_questions', ['survey' => $coursemodule]);


if($action == 'edit') {
    $customdata['action'] = 'edit';

    if($globalquestion->scalabtn == 1){
        $globalquestion = $DB->get_records('surveylight_questions', ['survey' => $coursemodule]);
    } else {
        $globalquestion = $DB->get_record('surveylight_questions', ['survey' => $coursemodule]);
    }

    $customdata['question'] = $globalquestion;

} else if($action =='create') {
    $customdata['action'] = 'create';
}

$PAGE->set_heading(format_string('Frage hinzufügen'));

$mform = new surveylight_form($formurl, $customdata);

if ($mform->is_cancelled()) {

    $redirectto = new moodle_url('/mod/surveylight/view.php?id=' . $id);
    redirect($redirectto,get_string('cancel_info', 'mod_surveylight') , null, \core\output\notification::NOTIFY_ERROR);

} else if ($fromform = $mform->get_data()) {

    if($fromform->action == 'create') {
        $createintro = "";

        $insert = new stdClass();

        $frage = "";
        $scalaoption = "";

        if($fromform->headerbtn == 1){
            $insert->headerbtn = $fromform->headerbtn;
        } else {
            $insert->headerbtn = 0;
        }

        foreach($fromform->option as $question) {
            if ($question != null) {
                $frage = $frage . $question . ";";
            }
        }

        $scalainsertbool = true;

        if($fromform->scalabtn == 1){
            foreach($fromform->scalaoption as $scalaterm){
                if($scalaterm != null){
                    if($scalainsertbool){
                        $insert->scalabtn = $fromform->scalabtn;
                        $insert->scalatitle = $scalaterm;
                        $insert->options = $frage;
                        $insert->survey = $fromform->id;
                        $insert->colorcode = $fromform->colorcode;
                        $insert->title = $fromform->title;
                        $createintro = $fromform->intro['text'];
                        $insert->intro = $createintro;
                        $insert->multi = $fromform->multi;
                        $DB->insert_record('surveylight_questions', $insert);
                        $scalainsertbool = false;
                    }else{
                        $insert->headerbtn = 1;
                        $insert->scalatitle = $scalaterm;
                        $insert->options = $frage;
                        $insert->survey = $fromform->id;
                        $insert->colorcode = $fromform->colorcode;
                        $insert->title = $fromform->title;
                        $createintro = $fromform->intro['text'];
                        $insert->intro = $createintro;
                        $insert->multi = $fromform->multi;
                        $DB->insert_record('surveylight_questions', $insert);
                    }
                }
            }
        } else if($fromform->offenefrage == 1){
            $insert->survey = $fromform->id;
            $insert->colorcode = $fromform->colorcode;
            $insert->title = $fromform->title;
            $insert->intro = $fromform->intro['text'];
            $insert->ofbtn = $fromform->offenefrage;
            $DB->insert_record('surveylight_questions', $insert);

        } else {
            $editintro = "";
            $insert->scalabtn = 0;
            $insert->options = $frage;
            $insert->survey = $fromform->id;
            $insert->colorcode = $fromform->colorcode;
            $insert->title = $fromform->title;
            $editintro = $fromform->intro['text'];
            $insert->intro = $editintro;
            $insert->multi = $fromform->multi;
            $DB->insert_record('surveylight_questions', $insert);
        }
        $redirectto = new moodle_url('/mod/surveylight/view.php?id=' . $fromform->id);
        redirect($redirectto, get_string('question_message_create', 'mod_surveylight'), null,\core\output\notification::NOTIFY_SUCCESS);
    } else if ($fromform->action == 'edit') {
        $DB->delete_records_select('surveylight_questions', 'survey =' . $fromform->id);
        $DB->delete_records_select('surveylight_answers', 'survey =' . $fromform->id);
        $update = new stdClass();
        $options2 = "";

        foreach($fromform as $frage2){
            if($frage2 != null){
                $options2 = $options2 . $frage2 . ";";
            }
        }

        $listofquestions = explode(";" , $options2);
        $answer = "";
        $newanswer = "";
        $scalaoption_update = "";
        $sizeofscala = sizeof($fromform->scalaoption);
        $startcount = 0;


        $bool = true;

        for($x = 0; $x<count($listofquestions);$x++){
            if($bool){
                if($fromform->option_repeats != $listofquestions[$x]){
                    $startcount++;
                } else {
                    $bool = false;
                }
            }
        }

        $dummy_listofquestion = $listofquestions;
        $dummy_bool = true;

        for($k = count($dummy_listofquestion); $k>0; $k--){

            if($dummy_listofquestion[$k] != null){
                if($dummy_bool) {
                    if ($dummy_listofquestion[$k] == 1) {
                        $dummy_listofquestion[$k] = "scalabtn";
                        $dummy_bool = false;
                    }
                }
            }
        }
        $dummy_bool = true;

        $dummy_count_start = 0;

        for($m = 0; $m<sizeof($dummy_listofquestion); $m++){
            if($dummy_listofquestion[$m] != null){
                if($dummy_bool){
                    if($dummy_listofquestion[$m] != "scalabtn"){
                        $dummy_count_start++;
                    }else{
                        $dummy_bool = false;
                    }
                }
            }
        }

        $dummy_listofquestion_option = $listofquestions;
        $dummy_bool_option = true;
        $dummy_count_start_option = 0;
        $dummy_count_end_option = 0;

        for($d = 0; $d<sizeof($dummy_listofquestion_option);$d++){
            if($dummy_bool_option){
                if($dummy_listofquestion_option[$d] != "Array"){
                    $dummy_count_start_option++;
                } else {
                    $dummy_bool_option = false;
                }
            }
        }

        $dummy_bool_option = true;

        for($dd = $dummy_count_start_option+1; $dd<sizeof($dummy_listofquestion_option);$dd++){
            if($dummy_bool_option){
                if($dummy_listofquestion_option[$dd] != "Array"){
                    $dummy_count_end_option++;
                }else {
                    $dummy_bool_option = false;
                }
            }
        }
        //what
        $dummy_count_end_option = $dummy_count_end_option + $dummy_count_start_option;

        if($fromform->scalabtn == 1){
            for($ix = $dummy_count_start_option+2; $ix<$dummy_count_end_option; $ix++){
                $answer = $answer . $listofquestions[$ix] . ";";
            }

            foreach($fromform->option as $newoption){
                if($newoption != null){
                    $newanswer = $newanswer . $newoption . ";";
                }
            }

            $final_answer = $answer . $newanswer;
            $old_scalaterm = "";

            for($n = $dummy_count_start+2; $n<sizeof($dummy_listofquestion)-4;$n++){
                if($dummy_listofquestion[$n] != null){
                    $old_scalaterm = $old_scalaterm . $dummy_listofquestion[$n] . ";";
                }
            }

            foreach($fromform->scalaoption as $update_scalaterm){
                if($update_scalaterm != null){
                    $scalaoption_update = $scalaoption_update . $update_scalaterm . ";";
                }
            }

            $old_scalaterm_final = explode(';', $old_scalaterm);
            $scalaheaderbtn = true;

            $new_parent_id = explode(';' , $fromform->parentid);
            $new_parent_id_counter = 1;

            $cleardb = true;
            for($ii = 0; $ii<sizeof($old_scalaterm_final)-1;$ii++){
                if($cleardb){
                    $cleardb = false;
                }
                if($scalaheaderbtn){
                    $update->id = $new_parent_id[0];
                    $update->survey = $fromform->id;
                    $update->colorcode = $fromform->colorcode;
                    $update->title = $fromform->title;
                    $update->intro = $fromform->intro['text'];
                    $update->multi = $fromform->multi;
                    $update->scalabtn = $fromform->scalabtn;
                    $update->scalatitle = $old_scalaterm_final[$ii];
                    $update->options = $final_answer;
                    $scalaheaderbtn =false;
                } else {
                    $update->id = $new_parent_id[$new_parent_id_counter];
                    $new_parent_id_counter++;
                    $update->headerbtn = 1;
                    $update->survey = $fromform->id;
                    $update->colorcode = $fromform->colorcode;
                    $update->title = $fromform->title;
                    $update->intro = $fromform->intro['text'];
                    $update->multi = $fromform->multi;
                    $update->options = $final_answer;
                    $update->scalabtn = $fromform->scalabtn;
                    $update->scalatitle = $old_scalaterm_final[$ii];
                }
                $DB->insert_record('surveylight_questions', $update);
            }

            $scalaoption_update_final = explode(';', $scalaoption_update);
            $changetoscalabtn = true;
            for($zz = 0; $zz<sizeof($scalaoption_update_final)-1;$zz++){
                if($old_scalaterm_final[$zz] != null){
                    $update->headerbtn = 1;
                    $update->scalabtn = $fromform->scalabtn;
                    $update->scalatitle = $scalaoption_update_final[$zz];
                    $update->options = $final_answer;
                    $update->survey = $fromform->id;
                    $update->colorcode = $fromform->colorcode;
                    $update->title = $fromform->title;
                    $editintro = $fromform->intro['text'];
                    $update->intro = $editintro;
                    $update->multi = $fromform->multi;
                    $DB->insert_record('surveylight_questions', $update);
                } elseif ($scalaoption_update_final[$zz] != null){
                    if($changetoscalabtn){
                        $update->headerbtn = 0 ;
                        $changetoscalabtn = false;
                    } else {
                        $update->headerbtn = 1 ;
                    }
                    $update->scalabtn = $fromform->scalabtn;
                    $update->scalatitle = $scalaoption_update_final[$zz];
                    $update->options = $final_answer;
                    $update->survey = $fromform->id;
                    $update->colorcode = $fromform->colorcode;
                    $update->title = $fromform->title;
                    $editintro = $fromform->intro['text'];
                    $update->intro = $editintro;
                    $update->multi = $fromform->multi;
                    $DB->insert_record('surveylight_questions', $update);
                }
            }

        } else {
            $elsecountstart = 7;

            if($fromform->headerbtn == 1){
                $update->headerbtn = $fromform->headerbtn;
                $elsecountstart = 8;
            } else {
                $elsecountstart = 7;
                $update->headerbtn = 0;
            }
            $update->scalabtn = 0;
            $update->id = $fromform->parentid;
            $update->survey = $fromform->id;
            $update->colorcode = $fromform->colorcode;
            $update->title = $fromform->title;
            $update->intro = $fromform->intro['text'];
            $update->multi = $fromform->multi;
            if($fromform->scalabtn == 1){
                $update->scalabtn = $fromform->scalabtn;
            }
            $update->scalabtn = 0;

            if($fromform->scalabtn != 1){
                for($i = $elsecountstart ; $i<=count($listofquestions)-7; $i++){
                    if($listofquestions[$i] != null){
                        $answer = $answer . $listofquestions[$i] . ";";
                    }
                }
            }else{
                for($i = 7 ; $i<=count($listofquestions)-7; $i++){
                    if($listofquestions[$i] != null){
                        $answer = $answer . $listofquestions[$i] . ";";
                    }
                }
            }

            $sum_answer = "";
            foreach($fromform->option as $fr){
                if($fr != null){
                    $sum_answer = $sum_answer . $fr . ";";
                }
            }

            $answerlist = $answer . $sum_answer;
            $update->options = $answerlist;
            $DB->insert_record('surveylight_questions', $update);
        }
        $redirectto = new moodle_url('/mod/surveylight/view.php?id=' . $fromform->id);
        redirect($redirectto, get_string('question_message_edit', 'mod_surveylight'),
            null, \core\output\notification::NOTIFY_SUCCESS);
    }
} else {

    echo $OUTPUT->header();
    $mform->display();
    echo $OUTPUT->footer();

}
