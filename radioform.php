<?php
/**
 *
 * @package        local_oc_grades
 * @author        Markus Strehling <markus.strehling@oncampus.de>
 * @license        http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");


class surveylight_radioform extends moodleform
{

    public function definition() {
        global $CFG, $DB;

        $mform = $this->_form; // Don't forget the underscore!

        $i = 0;
        $radioarray = array();
        foreach ($this->_customdata['answers'] as $ans) {

            if (!empty($ans)) {
                $radioarray[] = $mform->createElement('radio', "answer", '', $ans, $i, $attributes);
                $i++;
            }
        }
        $mform->addGroup($radioarray, 'answer', '', array(' '), false);
    }

    public function reset() {
        $this->_form->updateSubmission(null, null);
    }

}