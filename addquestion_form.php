<?php

//defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

class surveylight_form extends moodleform
{
    function definition()
    {
        global $CFG, $DB;
        $mform = $this->_form; // Don't forget the underscore!

        $coursemodule = $this->_customdata['coursemodule'];

        // What we need:
        /*
         * - ocsurvey:  parent id
         * - colorcode: color up your survey
         * - title:     title says it all
         * - intro:     general description
         * - multi:     checkbox/radio
         * - options:   available options, separated by ';'
         */

        $gn = $DB->get_record('surveylight_questions', ['survey' => $coursemodule]);

        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $this->_customdata['coursemodule']);

        $mform->addElement('hidden', 'action');
        $mform->setDefault('action', $this->_customdata['action']);

        $mform->addElement('html', '<h2>' . get_string('questiongeneral', 'mod_surveylight') . '</h2><br><br>');


        $mform->addElement('checkbox', 'headerbtn', get_string('headerbtn', 'mod_surveylight'), " ");
        $mform->setDefault('headerbtn', $this->_customdata['question']->headerbtn);
        $mform->addHelpButton('headerbtn', 'headerbtn', 'surveylight');

        $mform->addElement('checkbox', 'offenefrage', get_string('ofbtn', 'mod_surveylight'), " ");
        $mform->setDefault('offenefrage', $this->_customdata['question']->ofbtn);
        $mform->addHelpButton('offenefrage', 'offenefrage', 'surveylight');


        if($gn->scalabtn != 1){
            $mform->addElement('hidden', 'parentid');
            $mform->setDefault('parentid', $this->_customdata['question']->id);

            $colorcodes = array('#6db01f' => get_string('color_green', 'mod_surveylight'), '#f81' => get_string('color_orange', 'mod_surveylight'), '#000000' => get_string('color_black', 'mod_surveylight'), '#6495ed' => get_string('color_hellblau', 'mod_surveylight'));
            $mform->addElement('select', 'colorcode', get_string('questioncolorcode', 'mod_surveylight'), $colorcodes);
            $mform->setDefault('colorcode', $this->_customdata['question']->colorcode);
            $mform->addHelpButton('colorcode', 'colorcode', 'surveylight');

            $mform->addElement('text', 'title', get_string('questiontitle', 'mod_surveylight'));
            $mform->setDefault('title', $this->_customdata['question']->title);
            $mform->addHelpButton('title', 'title', 'surveylight');

            $mform->addElement('editor', 'intro', get_string('questionintro', 'mod_surveylight'));
            $mform->setDefault('intro', array('text'=> $this->_customdata['question']->intro));
            $mform->addHelpButton('intro', 'intro', 'surveylight');


            $mform->addElement('html', '<br><hr><br>');
            $mform->addElement('html', '<h2>' . get_string('questionquestion', 'mod_surveylight') . '</h2><br><br>');

            $options = array('radio' => get_string('simplequestion', 'mod_surveylight'), 'checkbox' => get_string('multiplechoice', 'mod_surveylight'));

            $mform->addElement('select', 'multi', get_string('questionmulti', 'mod_surveylight'), $options);
            $mform->setDefault('multi', $this->_customdata['question']->multi);


        } else {
            $gns = $DB->get_records('surveylight_questions', ['survey' => $coursemodule]);
            $testcount = $DB->count_records('surveylight_questions', ['survey' => $coursemodule]);
            $dcounter = 0;

            $dummy_scala = "";
            $dummy_bool = true;

            foreach($gns as $dummycount){
                $dcounter++;
            }

            foreach($gn as $test => $id){
                if($dummy_bool){
                    $dummy_scala = $dummy_scala . $id;
                    $dummy_bool = false;
                }
            }

            $dummy_id = "";
            $dummy_scalatitle = "";

            $scala_index_start = $dummy_scala;
            $scala_index_end = $dummy_scala + $dcounter;


            for($o = $scala_index_start; $o<$scala_index_end; $o++){
                $dummy_id = $dummy_id . $this->_customdata['question'][$o]->id . ';';
                $dummy_scalatitle = $dummy_scalatitle . $this->_customdata['question'][$o]->scalatitle . ';';
            }

            $mform->addElement('hidden', 'parentid');
            $mform->setDefault('parentid', $dummy_id);

            $colorcodes = array('#6db01f' => get_string('color_green', 'mod_surveylight'),
                '#f81' => get_string('color_orange', 'mod_surveylight'),
                '#000000' => get_string('color_black', 'mod_surveylight'),
                '#6495ed' => get_string('color_hellblau', 'mod_surveylight'));

            $mform->addElement('select', 'colorcode', get_string('questioncolorcode', 'mod_surveylight'), $colorcodes);
            $mform->setDefault('colorcode', $this->_customdata['question'][$scala_index_start]->colorcode);
            $mform->addHelpButton('colorcode', 'colorcode', 'surveylight');

            $mform->addElement('text', 'title', get_string('questiontitle', 'mod_surveylight'));
            $mform->setDefault('title', $this->_customdata['question'][$scala_index_start]->title);
            $mform->addHelpButton('title', 'title', 'surveylight');

            $mform->addElement('editor', 'intro', get_string('questionintro', 'mod_surveylight'));
            $mform->setDefault('intro', array('text'=> $this->_customdata['question'][$scala_index_start]->intro));
            $mform->addHelpButton('intro', 'intro', 'surveylight');

            $mform->addElement('html', '<br><hr><br>');
            $mform->addElement('html', '<h2>' . get_string('questionquestion', 'mod_surveylight') . '</h2><br><br>');

            $options = array('radio' => get_string('simplequestion', 'mod_surveylight'));

            $mform->addElement('select', 'multi', get_string('questionmulti', 'mod_surveylight'), $options);
            $mform->setDefault('multi', $this->_customdata['question'][$scala_index_start]->multi);
        }
        $mform->addHelpButton('multi', 'multi', 'surveylight');

        $answers = explode(";", $this->_customdata['question']->options);
        $scala_answers = explode(';', $gn->options);

        if($gn->scalabtn != 1){
            if($this->_customdata['action'] == "edit"){
                for($i = 0; $i<=100;$i++){
                    if($answers[$i] == "") {
                        $mform->addElement('hidden', 'options' . $i, "");
                        $mform->setDefault('options' . $i, $answers[$i]);
                    }else{
                        $mform->addElement('text', 'options'.$i, get_string('questions', 'mod_surveylight'));
                        $mform->setDefault('options'.$i , $answers[$i]);
                    }
                }
            }
        } else {
            if($this->_customdata['action'] == "edit"){
                for($i = 0; $i<=100;$i++){
                    if($scala_answers[$i] == "") {
                        $mform->addElement('hidden', 'options' . $i, "");
                        $mform->setDefault('options' . $i, $scala_answers[$i]);
                    }else{
                        $mform->addElement('text', 'options'.$i, get_string('questions', 'mod_surveylight'));
                        $mform->setDefault('options'.$i , $scala_answers[$i]);
                    }
                }
            }
        }

        $repeatarray = array();
        $repeatarray[] = $mform->createElement('text', 'option', get_string('survey_optionno', 'mod_surveylight'));
        $repeatarray[] = $mform->createElement('text', 'limit', get_string('survey_limitno', 'mod_surveylight'));

        $repeatarray = array();
        $repeatarray[] = $mform->createElement('text', 'option', get_string('questions', 'mod_surveylight'));
        $repeatarray[] = $mform->createElement('hidden', 'option', "" );

        if ($this->_instance){
            $repeatno = $DB->count_records('surveylight_questions', array('survey'=>$this->_instance));
            $repeatno += 2;
        } else {
            $repeatno = 5;
        }

        $repeateloptions = array();

        $repeateloptions['limit']['default'] = "yo";
        $repeateloptions['limit']['rule'] = 'numeric';
        $repeateloptions['limit']['type'] = PARAM_INT;
        $mform->setType('option', PARAM_CLEANHTML);
        $mform->setType('optionid', PARAM_INT);
        $this->repeat_elements($repeatarray, $repeatno,
            $repeateloptions, 'option_repeats', 'option_add_fields', 3, get_string('string_add_3', 'mod_surveylight'), true);


        $mform->addElement('html', '<br><hr><br>');
        $mform->addElement('html', '<h2>' . get_string('scalatitle', 'mod_surveylight') . '</h2><br><br>');

        if($gn->scalabtn != 1){
            $mform->addElement('checkbox', 'scalabtn', get_string('scalabtn', 'mod_surveylight'), " ");
            $mform->setDefault('scalabtn', $gn->scalabtn);
            $mform->addHelpButton('scalabtn', 'scalabtn', 'surveylight');
        } else {
            $mform->addElement('hidden', 'scalabtn');
            $mform->setDefault('scalabtn', $gn->scalabtn);
        }

        $scalaterm = explode(";", $dummy_scalatitle);

        if($this->_customdata['action'] == "edit"){
            for($si = 0; $si<=100; $si++){
                if($scalaterm[$si] == ""){
                    $mform->addElement('hidden', 'scalaoptions' . $si, "");
                    $mform->setDefault('scalaoptions' . $si, $scalaterm[$si]);
                }else{
                    $mform->addElement('text', 'scalaoptions'.$si, get_string('scalaterm', 'mod_surveylight'));
                    $mform->addHelpButton('scalaoption'.$si, 'scalaoption'.$si, 'mod_surveylight');
                    $mform->setDefault('scalaoptions'.$si , $scalaterm[$si]);

                }
            }
        }

        $scalarepeatarray = array();
        $scalarepeatarray[] = $mform->createElement('text', 'scalaoption', get_string('scalaoptionno', 'mod_surveylight'));
        $scalarepeatarray[] = $mform->createElement('text', 'scalalimit', get_string('scalalimitno', 'mod_surveylight'));


        $scalarepeatarray = array();
        $scalarepeatarray[] = $mform->createElement('text', 'scalaoption', get_string('scalaterm', 'mod_surveylight'));
        $scalarepeatarray[] = $mform->createElement('hidden', 'scalaoption', "" );

        if ($this->_instance){
            $scalarepeatno = $DB->count_records('surveylight_questions', array('survey'=>$this->_instance));
            $scalarepeatno += 2;
        } else {
            $scalarepeatno = 4;
        }

        $scalarepeateloptions = array();

        $scalarepeateloptions['limit']['default'] = "scala";
        $scalarepeateloptions['limit']['rule'] = 'numeric';
        $scalarepeateloptions['limit']['type'] = PARAM_INT;
        $mform->setType('scalaoption', PARAM_CLEANHTML);
        $mform->setType('scalaoptionid', PARAM_INT);
        $mform->addHelpButton('scalaoption', 'scalaoption', 'surveylight');
        $this->repeat_elements($scalarepeatarray, $scalarepeatno,
            $scalarepeateloptions, 'scalaoption_repeats', 'scalaoption_add_fields', 3, get_string('string_add_3', 'mod_surveylight'), true);



        $this->add_action_buttons();
        /*

                $mform->addElement('text', 'additional_lecturer', 'Zusätzliche Felder');
                $mform->setDefault('additional_lecturer', 0);
                $mform->setType('additional_lecturer', PARAM_INT);
                $mform->addRule('additional_lecturer', 'Bitte eine Zahl angeben', 'numeric', '', 'client');
                $mform->addElement('static', 'text_additional_lecturer', '', 'Bitte die Anzahl der zusätzlich benötigten Felder zum Anlegen weiterer Autor*innen und Anbieter*innen angeben.');
                $this->add_action_buttons($cancel = false, $submitlabel = 'Felder hinzufügen');
        */

    }

    function validation($data, $files)
    {
        return array();
    }

}
