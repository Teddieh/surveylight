<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_surveylight
 * @category    string
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Survey Light';
$string['modulename'] = 'Survey Light';
$string['surveylightname'] = 'Titel';
$string['surveylightname_help'] = "Bitte vergeben sie einen Titelnamen";
$string['surveylightfieldset'] = "surveylightfieldset";
$string['modulenameplural'] = "modulenameplural";
$string['surveylightsettings'] = "surveylightsettings";


$string['testtext'] = 'This is a Teststring';
$string['heading'] = 'Metadaten bearbeiten';
$string['heading_output'] = 'This is the Ouput Heading';
$string['title'] = 'Metadaten bearbeiten';

// Form content
$string['questiongeneral'] = 'Allgemeine Einstellungen';
$string['questioncolorcode'] = 'Farbe';
$string['questiontitle'] = 'Überschrift';
$string['questionintro'] = 'Beschreibung / Frage';
$string['questionmulti'] = 'Einfache / Mehrfache Auswahl';
$string['questionquestion'] = 'Einstellungen Frage / Abstufungen Skala';
$string['questionoption'] = 'Antwortmöglichkeit';
$string['headerbtn'] = 'Header ausblenden';
$string['ofbtn'] = 'Offene Frage einschalten';



$string['scalabtn'] = 'Bewertungsskala einschalten';
$string['scala'] = 'Likert-Skalen-Fragen';
$string['scalaterm'] = 'Item';
$string['scalatitle'] = 'Bewertungsskala';
$string['scalaoptionno'] = 'scaloption {no}';
$string['scalalimitno'] = 'scallimit {no}';

$string['color_green'] = 'grün';
$string['color_orange'] = 'orange';
$string['color_black'] = 'schwarz';
$string['color_hellblau'] = 'hellblau';

$string['survey_optionno'] = 'Option {no}';
$string['survey_limitno'] = 'Limit {sno}';
$string['questions'] = 'Antworten / Skala';

//dropdown addquestions_form
$string['simplequestion'] = 'Einfache Auswahl';
$string['multiplechoice'] = 'Mehrfache Auswahl';

$string['string_add_3'] = '3 Felder hinzufügen';

$string['addquestion_form_notification'] = 'Hier können Sie ihre Umfrage gestalten. [3/5]';


$string['group'] = 'Auswahl speichern';
$string['checkbox'] = 'Der Nutzer muss zum Abschluss seine Auswahl speichern.';

// Survey Message
$string['question_message_edit'] = 'Die Umfrage wurde aktualisiert.<br><br>Das ist nur eine Vorschau. Um auf die richtige Umfrage zu gelangen, gehen sie bitte auf, <strong>"Zurück zum Kurs"</strong> oder <strong>"Bestätigung"</strong>';
$string['question_message_create'] = 'Die Umfrage wurde erfolgreich erstellt.<br><br>Das ist nur eine Vorschau. Um auf die richtige Umfrage zu gelangen, gehen sie bitte auf <strong>"Zurück zum Kurs"</strong> oder <strong>"Bestätigung"</strong>.';

//view.php
$string['addquestion'] = "Frage hinzufügen";
$string['editquestion'] = "Fragen bearbeiten";
$string['submitbtn'] = "Bestätigung";
$string['backtocourse'] = "Zurück zum Kurs";
$string['viewdisplay'] = "Vorschau";
$string['newsurvey'] = 'Sie haben Surveylight erfolgreich erstellt.';
$string['newsurvey_addquestion'] = 'Bitte gehen Sie auf  "Frage hinzufügen", um Fragen hinzufügen.';
$string['cancel_info'] = 'Sie haben die Aktion abgebrochen.';


//locallib.php
$string['total_answer'] = "Antworten gesamt: ";
$string['headerbtn'] = "Header ausblenden";
//rule
$string['headerbtn_help'] = "Hier können Sie den farbigen Header mit Überschrift und Icon ausblenden, falls Sie einen eigenen Header verwenden möchten.";
$string['colorcode_help'] = "Legen Sie hier die Farbe des Headers fest.";
$string['title_help'] = "Geben Sie hier die Header-Überschrift an.";
$string['multi_help'] = "Einfache Antwort: <br>Nur eine Antwort ist auswählbar.<br><br>Mehrfach Auswahl:<br> Mehrere Antworten sind auswählbar.<br><br> Antwort/Skala:<br>Geben Sie hier die vorgegebenen Antworten an. Im Falle einer Skala geben Sie hier die Abstufungen der Skala an. Es sind beliebig viele Antworten / Abstufungen durch „Felder hinzufügen“ möglich.";
$string['intro_help'] = "Geben Sie hier eine Beschreibung bzw. eine Frage an.";
$string['scalabtn_help'] = "Hier können Sie die Verwendung einer Bewertungsskala mit beliebig vielen Items anstelle von Antworten aktivieren. <br>
                            <br>Item:<br>
                            Geben Sie hier die Items ein.";
$string['offenefrage_help'] = 'Hier können Sie die Verwendung einer offene Frage verwenden.<br><br> Bei Verwendung der offene Frage können keine Antwort / Skala und Bewertungsskala benutz werden.';

