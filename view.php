<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints an instance of mod_surveylight.
 *
 * @package     mod_surveylight
 * @copyright   2020 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/checkform.php');
require_once(__DIR__ . '/radioform.php');

// Course_module ID, or
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$o = optional_param('o', 0, PARAM_INT);

if ($id) {
    $cm = get_coursemodule_from_id('surveylight', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance = $DB->get_record('surveylight', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($o) {
    $moduleinstance = $DB->get_record('surveylight', array('id' => $n), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('surveylight', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid', mod_surveylight));
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);


$event = \mod_surveylight\event\course_module_viewed::create(array(
    'objectid' => $moduleinstance->id,
    'context' => $modulecontext
));

$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('surveylight', $moduleinstance);
$event->trigger();



$PAGE->set_url('/mod/surveylight/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);


echo $OUTPUT->header();


$questions = $DB->get_records('surveylight_questions', ['survey' => $moduleinstance->coursemodule]);

$addquestionbtn = new moodle_url('/course/view.php?id=' . $course->id);
echo '<a href="' . $addquestionbtn . '" class="btn"style=" color: #0f6cbf;text-decoration: underline #0f6cbf ">' . get_string('backtocourse', 'mod_surveylight') . '</a>&nbsp;';

if (!$questions) {
    $addquestionbtn = new moodle_url('/mod/surveylight/addquestion.php?id=' . $moduleinstance->id . '&cm=' . $moduleinstance->coursemodule);
    echo '<a href="' . $addquestionbtn . '" class="btn" style=" color: #0f6cbf;text-decoration: underline #0f6cbf ">' . get_string('addquestion', 'mod_surveylight') . '</a><br><br>';
    \core\notification::success(get_string('newsurvey', 'mod_surveylight'));
} else {
    $addquestionbtn = new moodle_url('/mod/surveylight/addquestion.php?action=edit&id=' . $moduleinstance->id . '&cm=' . $moduleinstance->coursemodule);
    echo '<a href="' . $addquestionbtn . '" class="btn" style=" color: #0f6cbf;text-decoration: underline #0f6cbf "> ' . get_string('editquestion', 'mod_surveylight') . '</a><br><br>';
}
    if($questions){
        echo '<h1 style="text-align: center; padding-top: 20px; padding-bottom: 40px;"> '.get_string('viewdisplay', 'mod_surveylight').'</h1><hr>';
    }

    foreach ($questions as $que) {
        if($que->headerbtn != 1){
            echo '<div class="introview" style="padding-top: 20px;"><h3>' . $que->title . '</h3></div>';
            echo '<div class="test" style="padding-bottom: 20px;"><p>' . $que->intro . '</p></div>';
        } else if ($que->headerbtn == 1 and $que->scalabtn != 1){
            echo  $que->intro;
        }

        $answers = explode(';', $que->options);
        $scalatotal = explode(';', $que->scalatitle);

        $formurl = new moodle_url('/mod/surveylight/view.php?id=' . $cm->id);

        $customdata = array('cmid' => $cm->id);
        $customdata['answers'] = $answers;
        $customdata['scalatitle'] = $scalatotal;
        $customdata['scalabtn'] = $que->scalabtn;

        if ($que->multi == 'radio') {
            echo '<div class="scala">'. $que->scalatitle .'</div>';
            $mform = new surveylight_radioform($formurl, $customdata);
        } else if ($que->multi == 'checkbox') {
            $mform = new surveylight_checkform($formurl, $customdata);
        }
            $mform->display();
        }


    if($questions){
        $btn = new moodle_url('/course/view.php?id=' . $course->id);
        echo '<hr><hr>';
        echo '<a href="' . $btn . '" class="btn" style="display: block; color: #0f6cbf;text-decoration: underline #0f6cbf  ">' . get_string('submitbtn', 'mod_surveylight') . '</a>&nbsp;';
        echo '<hr><hr>';
    }


echo $OUTPUT->footer();
 