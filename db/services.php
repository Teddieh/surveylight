<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_surveylight
 * @copyright   2019 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$functions = array(
    'mod_surveylight_survey' => array(          // Web service name (unique in all Moodle).
        'classname'   => 'mod_surveylight_external',     // Class containing the function implementation.
        'methodname'  => 'survey',              // Name of the function into the class.
        'classpath' => 'mod/surveylight/externallib.php',
        'description' => '',
        'type' => 'write',
        'ajax' => true,
        'loginrequired' => true,
    )
);

$services = array(
    'survey' => array(
            'functions' => array ('mod_surveylight_survey'),
            'restrictedusers' => 0,     // If 1, the administrator must manually select which user can use this service.
                                        // (Administration > Plugins > Web services > Manage services > Authorised users)
            'enabled' => 1,             // If 0, then token linked to this service won't work.
    )
);