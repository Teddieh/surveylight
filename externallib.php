<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_extjsmanager
 * @copyright   2019 oncampus GmbH <support@oncampus.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->libdir . "/completionlib.php");

class mod_surveylight_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function survey_parameters() {
        return new external_function_parameters(
            array(
                'answer' => new external_value(PARAM_TEXT, 'rating value'),
                'cmid' => new external_value(PARAM_INT, 'rating value'),
                'qid' => new external_value(PARAM_INT, 'rating value'),
            )
        );
    }

    public static function survey($answer, $cmid, $qid) {
        global $DB, $CFG, $USER;

        $params = self::validate_parameters(self::survey_parameters(), array('answer' => $answer, 'cmid' => $cmid, 'qid' => $qid));

        if($DB->record_exists('surveylight_answers', array('userid' => $USER->id, 'survey' => $cmid, 'question' => $qid))){
            $rec = $DB->get_record('surveylight_answers', array('userid' => $USER->id, 'survey' => $cmid, 'question' => $qid));

            $update = new stdClass();
            $update->id = $rec->id;
            $update->answer1 = $answer;

            $DB->update_record('surveylight_answers', $update);
        } else {
            $insert = new stdClass();
            $insert->userid = $USER->id;
            $insert->survey = $cmid;
            $insert->question = $qid;
            $insert->answer1 = $answer;
            $insert->answer2 = $answer;

            $DB->insert_record('surveylight_answers', $insert);
        }

        $answers = $DB->get_records('surveylight_answers', array('survey' => $cmid, 'question' => $qid));
        $question = $DB->get_record('surveylight_questions', array('id' => $qid));
        $options = explode(';', $question->options);

        $answercount = array();
        $answercount1 = array();


        foreach($options as $id => $option){
            $answercount[$id] = 0;
            $answercount1[$id] = 0;
        }

        $count = 0;

        foreach($answers as $answer){
            $answeroptions = explode(';', $answer->answer1);
            foreach($answeroptions as $option){
                $answercount[$option]++;
                $answercount1[$option]++;
                $count++;
            }
        }

        $answers = implode(';', $answercount);

        foreach($answercount as $id => $ac){
            $answercount[$id] = round(($ac / $count) * 100, 2);
            $answercount1[$id] = $ac;
        }

        $answers = implode(';', $answercount);
        $answers1 = implode(";", $answercount1);

        $sumanswers = 0;
        for($indexoftotal = 0; $indexoftotal<sizeof($answercount1); $indexoftotal++){
            $sumanswers = $sumanswers + $answercount1[$indexoftotal];
        }


        $cm = $DB->get_record('course_modules', array('id' => $cmid));
        $course = get_course($cm->course);
        $completion=new completion_info($course);

        if($completion->is_enabled($cm)) {
            $completion->update_state($cm, COMPLETION_COMPLETE);
        }

        $retarray = array(
            'answers' => "$answers",
            'answers1' => "$answers1",
            'sumanswers' =>"$sumanswers",
            'answerspercent' => "$answerspercent",
            'count' => $count,
            'options' => $question->options,
        );

        return $retarray;
    }

    /**
     * Returns description of method result value
     * @return external_value
     */
    public static function survey_returns() {
        return new external_function_parameters(
            array(
                'answers' => new external_value(PARAM_TEXT, 'id of entry'),
                'answers1' => new external_value(PARAM_TEXT, 'id of entry'),
                'sumanswers' => new external_value(PARAM_TEXT, 'id of entry'),
                'answerspercent' => new external_value(PARAM_TEXT, 'id of entry'),
                'count' => new external_value(PARAM_TEXT, 'id of entry'),
                'options' => new external_value(PARAM_TEXT, 'id of entry'),
            )
        );
    }
}